from peewee import *

database = SqliteDatabase('BlackLee.db')

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Guid(BaseModel):
    g_uuid = BlobField(index=True)
    uid = AutoField()
    uid_name = CharField(index=True)
    uid_raw = TextField()
    uid_type = CharField()

    class Meta:
        table_name = 'guid'

class Include(BaseModel):
    inc_type = CharField()
    inc_value = CharField()
    uid = AutoField()

    class Meta:
        table_name = 'include'

class Inf(BaseModel):
    binaries = TextField(null=True)
    binaries_ia32 = TextField(column_name='binaries.ia32', null=True)
    binaries_x64 = TextField(column_name='binaries.x64', null=True)
    buildoptions = TextField(null=True)
    buildoptions_arm = TextField(column_name='buildoptions.arm', null=True)
    defines = TextField(null=True)
    defines_aarch64 = TextField(column_name='defines.aarch64', null=True)
    defines_arm = TextField(column_name='defines.arm', null=True)
    defines_ia32 = TextField(column_name='defines.ia32', null=True)
    defines_x64 = TextField(column_name='defines.x64', null=True)
    depex = TextField(null=True)
    depex_aarch64 = TextField(column_name='depex.aarch64', null=True)
    depex_arm = TextField(column_name='depex.arm', null=True)
    depex_common_dxe_driver = TextField(column_name='depex.common.dxe_driver', null=True)
    depex_common_dxe_runtime_driver = TextField(column_name='depex.common.dxe_runtime_driver', null=True)
    depex_common_dxe_sal_driver = TextField(column_name='depex.common.dxe_sal_driver', null=True)
    depex_common_dxe_smm_driver = TextField(column_name='depex.common.dxe_smm_driver', null=True)
    depex_common_peim = TextField(column_name='depex.common.peim', null=True)
    depex_common_uefi_driver = TextField(column_name='depex.common.uefi_driver', null=True)
    depex_ia32 = TextField(column_name='depex.ia32', null=True)
    depex_x64 = TextField(column_name='depex.x64', null=True)
    featurepcd = TextField(null=True)
    featurepcd_aarch64 = TextField(column_name='featurepcd.aarch64', null=True)
    featurepcd_arm = TextField(column_name='featurepcd.arm', null=True)
    featurepcd_common = TextField(column_name='featurepcd.common', null=True)
    featurepcd_ia32 = TextField(column_name='featurepcd.ia32', null=True)
    featurepcd_x64 = TextField(column_name='featurepcd.x64', null=True)
    fixedpcd = TextField(null=True)
    fixedpcd_common = TextField(column_name='fixedpcd.common', null=True)
    guids = TextField(null=True)
    guids_ia32 = TextField(column_name='guids.ia32', null=True)
    guids_x64 = TextField(column_name='guids.x64', null=True)
    libraryclasses = TextField(null=True)
    libraryclasses_aarch64 = TextField(column_name='libraryclasses.aarch64', null=True)
    libraryclasses_arm = TextField(column_name='libraryclasses.arm', null=True)
    libraryclasses_ia32 = TextField(column_name='libraryclasses.ia32', null=True)
    libraryclasses_x64 = TextField(column_name='libraryclasses.x64', null=True)
    packages = TextField(null=True)
    packages_aarch64 = TextField(column_name='packages.aarch64', null=True)
    packages_arm = TextField(column_name='packages.arm', null=True)
    packages_ia32 = TextField(column_name='packages.ia32', null=True)
    packages_x64 = TextField(column_name='packages.x64', null=True)
    pcd = TextField(null=True)
    pcd_aarch64 = TextField(column_name='pcd.aarch64', null=True)
    pcd_arm = TextField(column_name='pcd.arm', null=True)
    pcd_common = TextField(column_name='pcd.common', null=True)
    pcd_ia32 = TextField(column_name='pcd.ia32', null=True)
    pcd_x64 = TextField(column_name='pcd.x64', null=True)
    pcdex = TextField(null=True)
    ppis = TextField(null=True)
    ppis_ia32 = TextField(column_name='ppis.ia32', null=True)
    protocols = TextField(null=True)
    protocols_common = TextField(column_name='protocols.common', null=True)
    protocols_x64 = TextField(column_name='protocols.x64', null=True)
    sources = TextField(null=True)
    sources_aarch64 = TextField(column_name='sources.aarch64', null=True)
    sources_arm = TextField(column_name='sources.arm', null=True)
    sources_common = TextField(column_name='sources.common', null=True)
    sources_ebc = TextField(column_name='sources.ebc', null=True)
    sources_ia32 = TextField(column_name='sources.ia32', null=True)
    sources_riscv64 = TextField(column_name='sources.riscv64', null=True)
    sources_x64 = TextField(column_name='sources.x64', null=True)
    userextensions_tianocore_extrafiles_ = TextField(column_name='userextensions.tianocore.-extrafiles-', null=True)

    class Meta:
        table_name = 'inf'
        primary_key = False

class Libs(BaseModel):
    lib_name = CharField()
    lib_type = CharField()
    lib_value = CharField()
    uid = AutoField()

    class Meta:
        table_name = 'libs'

class Pcd(BaseModel):
    pcd_id = IntegerField(index=True)
    pcd_name = CharField(index=True)
    pcd_ntype = CharField()
    pcd_pkg = CharField()
    pcd_raw = TextField()
    pcd_type = CharField()
    pcd_uid = BlobField(primary_key=True)
    pcd_value = TextField()

    class Meta:
        table_name = 'pcd'

