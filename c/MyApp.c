#include <stdio.h>
void add(double a[][2])
{
    a[2][0] = a[0][0] + a[1][0];
    a[2][1] = a[0][1] + a[1][1];
}
int DogD(int day)
{

    if (day == 1)
    {
        return 1;
    }

    return (DogD(day - 1) + 1) * 2;
}
int Xn(int x)
{
    if (x == 0)
    {
        return 1;
    }
    return Xn(x - 1) + x;
}
int setMapTable(int x, int y, int MapTable[][8])
{
    if (x >= 0 && x < 8 && y >= 0 && y < 8)
    {
        int min = x > y ? y : x;
        int markR = x + y;
        int markL = x - y + 8;

        MapTable[x][y] = 1;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (i == x)
                    MapTable[i][j] = -1;
                if (j == y)
                    MapTable[i][j] = -1;
                if (markR == (i + j))
                    MapTable[i][j] = -1;
                if (markL == (i - j + 8))
                    MapTable[i][j] = -1;
            }
        }
    }
    return 1;
}
void PrintSqure(int MapTable[][8])
{
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%02d ", MapTable[i][j]);
        }
        printf("\n");
    }
}
int Quee()
{
    int MapTable[8][8] = {};
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            MapTable[i][j] = 1;
        }
    }

    int row = 0, col = 0;
    MapTable[row][col] = 1;
    setMapTable(0, 0, &MapTable[0]);
    PrintSqure(&MapTable[0]);
    return 1;
}

int main()
{
    int AgeArray[4] = {11,
                       12,
                       13,
                       14};
    int *IntPtr = NULL;
    IntPtr = AgeArray;
    printf("AgeArray Addr : %p \n", &AgeArray[1]);
    for (int i = 0; i < 4; i++)
    {
        printf("Age : %d\n", IntPtr[i]);
    }
    // 7.1.2
    double a[3][2] = {1, 1, 2, 2};
    // Warning
    // add(&a[0]);
    add(&a[0]);
    printf("[7.1.2] %lf+%lfi\n", a[2][0], a[2][1]);
    // 7.5.1
    printf("[7.5.1] %d \n", DogD(10));
    // 7.5.2
    printf("[7.5.2] Xn() %d\n", Xn(100));
    // 7.5.3
    printf("[7.5.3] Quee() \n");
    Quee();
}