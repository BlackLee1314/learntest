#include <iostream>
#include <cstring>
#include <string>
#include <wchar.h>
#include <termio.h>
using namespace std;
int fun();
int getch(void);

int main()
{
  char str[20];

  cout << "My main Func" << endl;
  fun();
}

int fun()
{
  wchar_t password[21];
  wchar_t TempString[20];
  wchar_t BufferedString[20 + 5];
  wchar_t Key;
  char    tKey;
  wchar_t KeyPad[2];
  int    passwordOK;
  int    pdlen;
  int    Index = 0, Index2 = 0;
  unsigned int CurrentCursor = 0;
  memset(BufferedString, L' ', sizeof(BufferedString));
  memset(password, 0x00, sizeof(password));
  passwordOK = false;
  BufferedString[20 + 4] = 0x0000;
  cout << "Fun start:" << endl;
  do
  {
    tKey = getch();
    Key = wchar_t(tKey);

    // cout << "tKey Unicode: " << int(tKey) <<endl;
    // cout << " Key Unicode: " << int(Key) <<endl;
    switch (Key)
    {
    case 27:
      if (Key == 27)
      {
        passwordOK = -1;
        // cout << "Input is NULL" << endl;
        // jump out
      }
      break;

    case 13: //enter
      cout << "\n\n\n\n";
      if (wcslen(password)*4 >= sizeof(wchar_t))
      {
        passwordOK = 1;
      }
      break;

    case 8:
      // cout << "BackSpace Input" << endl;
      if (password[0] != 0 && CurrentCursor != 0)
      {
        for (Index = 0; Index < CurrentCursor - 1; Index++)
        {
          TempString[Index] = password[Index];
        }

        pdlen = wcslen(password);
        // cout << "PASSWORD LEN:" << pdlen << endl;
        if (pdlen >= CurrentCursor)
        {
          for (Index = CurrentCursor - 1, Index2 = CurrentCursor; Index2 < pdlen; Index++, Index2++)
          {
            TempString[Index] = password[Index2];
          }
          TempString[Index] = 0;
        }
        //
        // Effectively truncate string by 1 character
        //
        wmemcpy(password, TempString, wcslen(TempString) + 1);
        CurrentCursor--;
      }

    default:
      cout << "                                                        \n                                                        \n                                                        \n                                                        \n";
      cout << flush;
      cout <<"\033[4A";

      //
      // Check invallid char.
      //
      pdlen = wcslen(password);

      if (8 == Key)
      {  //remove one key
        if (password[0] == 0)
        {
          memset(BufferedString, L' ', sizeof(BufferedString));
          BufferedString[20 + 4] = 0;
          // wcout << L"BUFFER:" << BufferedString << endl;
          break;
        }
        else
        {
          BufferedString[CurrentCursor] = L' ';
        }
        // if input normal word
      }
      else if (pdlen < 20)
      {      // Add new char.
        KeyPad[0] = Key;
        KeyPad[1] = 0x00;
        wcscat(password, KeyPad);
        // fill cursor *
        BufferedString[CurrentCursor] = L'*';
        CurrentCursor++;
      }
      cout << "\033[37m";
      wcout << L"\rPASSWD: " << password << L"\nBUFFER: " << BufferedString << L"\nPDLEN : " << wcslen(password) << "\nCursor: " << CurrentCursor << endl;
      cout << "\033[30m";
      cout << "\033[4A";
      break;
    }
    if (passwordOK < 0)
    {
      // cout << "ESC inputed" <<endl;
      break;
    }
    else if (passwordOK > 0)
    {
      // cout << "Enter inputed" <<endl;
      break;
    }
  } while (true);
  cout << "Fun over" << endl;
  return 0;
}

int getch(void)
{
  struct termios tm, tm_old;
  int fd = 0, ch;

  if (tcgetattr(fd, &tm) < 0)
  {//保存现在的终端设置
    return -1;
  }

  tm_old = tm;
  cfmakeraw(&tm);//更改终端设置为原始模式，该模式下所有的输入数据以字节为单位被处理
  if (tcsetattr(fd, TCSANOW, &tm) < 0)
  {//设置上更改之后的设置
    return -1;
  }

  ch = getchar();
  if (tcsetattr(fd, TCSANOW, &tm_old) < 0)
  {//更改设置为最初的样子
    return -1;
  }

  return ch;
}