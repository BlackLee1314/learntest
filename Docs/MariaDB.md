# MariaDB Install

Notice recommand use cli to install, gui will not show the initial notice.

`# apt install mariadb-server`

# Initialize mariadb 
`sudo service mysql start`  
`sudo mysql_secure_installation`  
_这里注意 root 密码设置时选择了设置之后，使用 `mysql -u root -p` 输入正确密码还是会登录不上这是因为默认的plugin值为unix_socket,需要更改为mysql_native_password来解决_  
`sudo mysql`  
```sql
MariaDB[none]> use mysql;
MariaDB[none]> select host,password,user from user;
+-----------+-------------+----------+
| Host      | User        | Password |
+-----------+-------------+----------+
| localhost | mariadb.sys |          |
| localhost | root        | invalid  |
| localhost | mysql       | invalid  |
| localhost |             |          |
| tardis    |             |          |
+-----------+-------------+----------+
5 rows in set (0.001 sec)
MariaDB[none]> set password for root@localhost =PASSWORD('password');
MariaDB[none]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
MariaDB[none]> select host,password,user from user;
+-----------+-------------+-------------------------------------------+
| Host      | User        | Password                                  |
+-----------+-------------+-------------------------------------------+
| localhost | mariadb.sys |                                           |
| localhost | root        | *4E3588B1FC4E67BEF9C3EA8A9473010DDB78F357 |
| localhost | mysql       | invalid                                   |
| localhost |             |                                           |
| tardis    |             |                                           |
| %         | blackDB     | *B2B366CA5C4697F31D4C55D61F0B17E70E5664EC |
+-----------+-------------+-------------------------------------------+
6 rows in set (0.002 sec)
```
`# mysql -u root -p 04569252`  


# Mariadb Operate
```sql
MariaDB [mysql]> create database MineDB;
MariaDB [mysql]> CREATE TABLE IF NOT EXISTS `TestTable`(
                `runoob_id` INT UNSIGNED **AUTO_INCREMENT**,
                `runoob_title` VARCHAR(100) NOT NULL,
                `runoob_author` VARCHAR(40) NOT NULL,
                `submission_date` DATE,
                PRIMARY KEY ( `runoob_id` )
                )ENGINE=InnoDB DEFAULT CHARSET=utf8;
MariaDB [mysql]> flush privileges;
#create user
create user zhangsan identified by 'zhangsan';
#update item
UPDATE table_name
SET column1=value1,column2=value2,...
WHERE some_column=some_value;
```
# ErrorLog
1. - ERROR 1045 (28000): Access denied for user 'blacklee'@'localhost' (using password: YES)  
   - Reason:  
     - Cause there exsit a user ''@'localhost' before your username, so the mariadb matched user ''@'localhost', drop user ''@localhost..
     - Cause the login plugin is set to unix_socket, just change it to mysql_native_password for solve it.   
```sql
MariaDB [mysql]> select plugin,user from user;
+-----------------------+----------+
| plugin                | user     |
+-----------------------+----------+
| mysql_native_password | root     |
| unix_socket           | blacklee |
+-----------------------+----------+
2 rows in set (0.000 sec)
```