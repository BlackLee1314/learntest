# Git Useage

## Repo

### Upload local repo to remote
1. init your project  
   `git init`
2. stage & commit to local git repo  
   `git add .`  
   `git commit -m "commit msg"`
3. create remote your remote repo
4. set your local repo's remote
   `git add remote origin git@bitbucket.org:BlackLee1314/kivy_myapp.git`  
5. pull remote and rebase local  
   `git pull --rebase orgin master`
6. push local to remote  
   `git push -u orgin master`