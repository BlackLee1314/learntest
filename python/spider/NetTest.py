from json import detect_encoding
from threading import Thread
import sys
import re
import chardet
import requests
from lxml import etree
from pprint import pprint
from NovelModel import *
import time
import grequests
chapter_queue_size = 3000
book_queue_size = 200

host = "https://www.laidudu.com"
searchurl = "/read/search/"
cookies = None


def GetSitePages():
    res = requests.get(host)
    # cookies = res.cookies
    res = etree.HTML(res.content)
    allurl = res.xpath('//@href')
    sort_re = re.compile(r"/dusort(\d)/(\d+).html")
    sort_dict = {}
    r_list = []
    for url in allurl:
        if(sort_re.match(url)):
            r_list.append(grequests.get(host+url))
    r_list = r_list[:7]
    r_list = grequests.map(r_list)
    for r in r_list:
        if r:
            res = etree.HTML(r.content)
            last_page = res.xpath('//a[@class="a-btn"]')[-1].get('href')
            sort, page = sort_re.match(last_page).groups()
            sort_dict[sort] = page
    return sort_dict


def ParseSortBooks(sort: int, page: int):
    theurl = f"\dusort{sort}\{page}.html"
    # res = requests.get(host+theurl)


def ParseSearchResult(content: bytes):
    '''Parse html content to booklist

    return Tuple((name,url,author,update,updateurl))
    '''
    html = etree.HTML(content)
    booknamelist = html.xpath('//h4/a/text()')
    bookurllist = html.xpath('//h4/a/@href')
    bookauthorlist = html.xpath('//div[@class="author"]/text()')
    bookupdatelist = html.xpath('//div[@class="update"]/a/text()')
    bookupdateurllist = html.xpath('//div[@class="update"]/a/@href')
    return zip(booknamelist, bookurllist, bookauthorlist, bookupdatelist, bookupdateurllist)


def PrintSearchResult(seach_res):
    print(f"_"*64)
    print(f"|Index|Name{' '*21}|Author{' '*19}|State{' '*20}|")
    for index, item in enumerate(seach_res):
        print(
            f"|{index:0>5}|{item[0]:25}|{item[1]:25}|{item[3]:25}|\n")
    print(f"_"*64)


def BookSerch(data: dict):
    '''post search with parameter

    if request error return Error,
    if NO book found return None
    '''
    try:
        res = requests.post(host+searchurl, data=data)
        booklist = list(ParseSearchResult(res.content))
        if(booklist):
            return booklist
        return None
    except:
        return 'Error'


def ParsBook(content: bytes):
    bookinfo = {}
    html = etree.HTML(content)
    ddts = html.xpath('//dd/a/text()')
    ddurls = html.xpath('//dd/a/@href')

    # put in queue
    # chapter_down_queue.put()

    c_list = list(zip(ddts, ddurls))
    # updatelist = c_list[:12]
    c_list = c_list[12:]
    infos = html.xpath('//div[@class="info"]//text()')
    bookinfo['name'] = infos[2]
    bookinfo['author'] = infos[4].split('：')[-1]
    bookinfo['categary'] = infos[5].split('：')[-1]
    bookinfo['state'] = infos[6].split('：')[-1]
    bookinfo['b_bytes'] = infos[7].split('：')[-1]
    bookinfo['updatetime'] = infos[8].split('：')[-1]
    bookinfo['latest'] = infos[10]
    bookinfo['intro'] = infos[13].replace(
        '\n', '').replace('\t', '').replace(' ', '')
    bookinfo['chapters'] = list(map(Chapter2Dict, c_list))
    return bookinfo


def BookGet(url: str):
    '''post search with parameter

    if request error return Error,
    if NO book found return None
    '''
    try:
        res = requests.get(url=host + url)
        bookinfo = ParsBook(res.content)
        # /book/24033/
        bookinfo['uid'] = int(url.split('/')[-2])
        return bookinfo
    except:
        return 'Error'


def ChapterGet(url: str):
    try:
        res = requests.get(url=host + url)
        html = etree.HTML(res.content)
        content = html.xpath('//div[@id="content"]/text()')[1:-1]
        content = ''.join(content).replace('\r', '').replace(
            '\n', '').replace('\xa0'*4, '\n')
        return content
    except:
        print(f"GET {url} STATUS : {res.status_code}\n")
        print("Retry:")
        return 'Error'


def Chapter2Dict(chapter: tuple):
    d_chapter = {}
    d_chapter['name'] = chapter[0]
    d_chapter['text'] = ''
    d_chapter['url'] = chapter[1]
    d_chapter['uid'] = chapter[1].split('/')[-1].split('.')[0]
    d_chapter['bookid'] = chapter[1].split('/')[2]
    return d_chapter


def Book2Dict():
    pass


def DownloadBook(url: str):
    book = BookGet(url=url)
    chapterinfos = book.get('chapters')
    del book['chapters']

    if(Book.get_by_id('uid')):
        Book.save(book)
    else:
        SaveBook(book)

    Num_Chapters = len(chapterinfos)
    bookname = book['name']
    count = 0
    chapters = list(map(Chapter2Dict, chapterinfos))
    SaveChapters(chapters)
    for chapter in chapters:
        # if(Chapter.get_or_none('text'=''))
        content = ChapterGet(chapter.get('url'))
        chapter['text'] = content
        chapter_queue.put(chapter)
        SaveChapter()
        # book['contents'].append(content)
        # with open("Books.txt", mode='a') as fd:
        #     fd.write(f"\n{c_name}\n")
        #     fd.write(content)
        count = count + 1
        print(
            f"\r[BOOK]: {book['name']} P:({count}/{Num_Chapters}) DOWNLOADED: {chapter['name']} ")
        time.sleep(0.5)
    print('\n')
    return book


def Search_Down_Books():
    print(f"_"*64+'\n')
    searchdata = {}
    print("Start Boot DownLoad \n")
    searchdata['searchkey'] = ""
    while(True):
        name = input("小说名称:")
        searchdata['searchkey'] = name
        if(name == '00'):
            break
        res = BookSerch(searchdata)
        PrintSearchResult(res)
        index = input("输入要下载的小说编号：")
        try:
            index = int(index)
        except:
            print("输入错误,请重新输入:")
        DownloadBook(res[index][1])


if __name__ == '__main__':
    url = "/read/search/"
    # Clear_Table_Value()
    # Search_Down_Books()
    GetHomePage()
    print('END')
