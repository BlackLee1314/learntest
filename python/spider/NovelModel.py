from peewee import *
from queue import Queue

db_name = "BlackLee.db"
db = SqliteDatabase(db_name)
chapter_queue = Queue(5000)
book_queue = Queue(10)


class Chapter(Model):
    uid = IntegerField(primary_key=True)
    bookid = IntegerField()
    url = CharField()
    name = CharField()
    text = TextField()

    class Meta:
        database = db


class Book(Model):
    uid = IntegerField(primary_key=True)
    name = CharField()
    author = CharField()
    categary = CharField(null=True)
    state = CharField(null=True)
    b_bytes = CharField(null=True)
    updatetime = CharField(null=True)
    latest = CharField(null=True)
    intro = TextField(null=True)

    class Meta:
        database = db


def Create_Tables():
    db.connect()
    db.create_tables([Chapter, Book])
    db.close()


def Clear_Table_Value():
    Chapter.delete().execute()
    Book.delete().execute()


def SetChapterText(c_uid: int, c_dict: dict):
    Chapter.set_by_id(c_uid, c_dict)


def SaveChapters(chapters: list):
    with db.atomic():
        Chapter.insert_many(chapters).execute()
    return 0


def SaveChapter():
    while(not chapter_queue.empty()):
        raw_chapter = chapter_queue.get()
        name = raw_chapter.get('name')
        chapter = Chapter(**raw_chapter)
        try:
            chapter.save()
        except:
            print(f"Save [{name}]Error")
    return 0


def SaveBook(book: dict):
    name = book.get('name')
    try:
        Book.create(**book)
        print(f"[BOOK] {name} SAVED\n")
    except Exception:
        print(f"[ERROR] {name} SAVE Error {Exception}\n")
    return 0


if __name__ == '__main__':
    Create_Tables()
    c1 = {'uid': 123,
          'name': 'testname',
          'bookid': 343,
          'url': '/343/123/',
          'text': 'texttexttext'
          }
    chapter_queue.put(c1)
    SaveChapter()
    b1 = {
        'uid': 111,
        'name': 'bookname',
        'author': 'Lee',
        'categary': 'cate',
        'state': 'done',
        'b_bytes': '234234',
        'updatetime': '1234234324',
        'latest': 'ggggg',
        'intro': 'desc'
    }
    book_queue.put(b1)
    SaveBook()
