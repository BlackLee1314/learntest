import grequests
from lxml import etree
import re
import os
from peewee import PeeweeException
import requests
from scrapy.http.response import text
from NovelModel import *
from NetTest import *
import pickle
host = "https://www.laidudu.com"


def save_pkl(path, obj):
    pickle_file = open(path, 'wb')
    pickle.dump(obj, pickle_file)
    pickle_file.close()
    print("保存成功")


def load_pkl(path):
    pickle_file = open(path, 'rb')
    obj = pickle.load(pickle_file)
    pickle_file.close()
    print("读取成功")
    return obj


a = [1, 2, 3, 4]
save_pkl("./list_a.pkl", a)


def GetHomePage():
    res = requests.get(host)
    # cookies = res.cookies
    res = etree.HTML(res.content)
    allurl = res.xpath('//@href')
    sort_re = re.compile(r"/dusort(\d)/(\d+).html")
    sort_dict = {}
    r_list = []
    for url in allurl:
        if(sort_re.match(url)):
            r_list.append(grequests.get(host+url))
    r_list = r_list[:7]
    r_list = grequests.map(r_list)
    for r in r_list:
        if r:
            res = etree.HTML(r.content)
            last_page = res.xpath('//a[@class="a-btn"]')[-1].get('href')
            sort, page = sort_re.match(last_page).groups()
            sort_dict[sort] = page
    return sort_dict


def GetAllBooks1(siteinfo: dict):
    url = host + '/dusort{}/{}.html'
    # req_list = []
    for sort, max_page in siteinfo.items():
        if('res_'+sort+'.plk' in os.listdir()):
            res_list = load_pkl('res_'+sort+'.plk')
        else:
            max_page = int(max_page)
            urllist = [url.format(sort, x) for x in range(1, max_page+1)]
            req_list = [grequests.get(url) for url in urllist]
            res_list = grequests.map(req_list)
            save_pkl('res_'+sort+'.plk', res_list)
        for reso in res_list:
            if reso.status_code != 200:
                continue
            print(reso.url)
            res = etree.HTML(reso.content)
            name_list = res.xpath('//span[@class="s2"]//text()')
            latest_list = res.xpath('//span[@class="s3"]/a')
            latest_list = [
                latest.text if latest.text else '' for latest in latest_list]
            author_list = res.xpath('//span[@class="s4"]')
            author_list = [
                author.text if author.text else '' for author in author_list]
            uptime_list = res.xpath(
                '//div[@class="l bd"]/ul/li/span[@class="s5"]')
            uptime_list = [
                uptime.text if uptime.text else '' for uptime in uptime_list]
            url_list = res.xpath(
                '//div[@class="l bd"]/ul/li/span[@class="s2"]/a/@href')
            uid_list = [int(s.split('/')[-2]) for s in url_list]
            book_list = list(zip(name_list, latest_list,
                                 author_list, uptime_list, uid_list))
            try:
                with db.atomic():
                    Book.insert_many(book_list, fields=[
                        Book.name, Book.latest, Book.author, Book.updatetime, Book.uid]).execute()
            except IntegrityError:
                for index, uid in enumerate(uid_list):
                    if(not Book.get_or_none(uid=uid)):
                        Book.insert(name=book_list[index][0],
                                    latest=book_list[index][1],
                                    author=book_list[index][2],
                                    updatetime=book_list[index][3],
                                    uid=book_list[index][4]).execute()


def GetAllBooks():
    url = '/book/{}/'
    r_list = []
    max_bookid = 75000
    group_size = 300
    page_count = int(max_bookid/group_size)
    for count in range(page_count):
        url_list = [host+url.format(x+count*group_size)
                    for x in range(group_size)]
        req_list = [grequests.get(url) for url in url_list]
        res_list = grequests.map(req_list)
        for index, res in enumerate(res_list):
            cuid = index+count*group_size
            if res.status_code != 200:
                continue
            book = ParsBook(res.content)
            print(f"CUID:{cuid}    NAME:{book.get('name')}")
            chapters = book.get('chapters')
            book['uid'] = cuid
            del book['chapters']
            try:
                Book.insert(**book).execute()
            except:
                del book['uid']
                Book.update(**book).where(Book.uid == cuid).execute()

            try:
                with db.atomic():
                    Chapter.insert_many(chapters).execute()
            except:
                for chapter in chapters:
                    puid = chapter.get('uid')
                    if not Chapter.get_or_none(uid=puid):
                        Chapter.insert(chapter)
                    else:
                        del chapter['uid']
                        Chapter.update(
                            **chapter).where(Chapter.uid == puid).execute()


if __name__ == '__main__':
    # a = [grequests.get(host)]
    # grequests.map(a)
    # d = GetHomePage()
    # GetAllBooks1(d)
    GetAllBooks()
