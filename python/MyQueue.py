import queue
import threading
import time
import wget
import requests
import sqlite3
import sys
import os
import re
from scrapy import Selector

DBfile = 'MyDB.db'
WriteCommand = "INSERT INTO uu234wQ (uu234wID,NovelName) VALUES (?,?);"
NovelRe =  re.compile(r"http://www.uu234w.com/(b\d*)/")
Tag = 0

GetUrlQueue = queue.Queue(500)
ParseQueue = queue.Queue(1000)
SqlQueue = queue.Queue(100)
WgetQueue = queue.Queue(100000)
UrlList = ["http://www.uu234w.com/quanben/list_"+str(x)+".html" for x in range(1,432)]
for url in UrlList:
    GetUrlQueue.put(url)

def WgetFile(url=None):
    # print(WgetFile.__name__, end='\n')
    if url:
        return "No args found\n"
    global Tag
    global WgetQueue
    Wcount = 0
    Wlock = threading.Lock()
    while Tag != 3:
        Wlock.acquire()
        if WgetQueue.empty():
            time.sleep(3)
            continue
        url = WgetQueue.get()
        try:
            wget.download(url, "./txt/"+url.split('/')[-1])
            time.sleep(3)
        except :
            with open('WgetFail.log','a+') as fp :
                fp.write(url+'\n')
        finally: 
            Wcount+=1
            print("\n[WGET]: "+url+" WgetCount : "+str(Wcount)+"\n")
        Wlock.release()

    print("[WGET]: WgetQueue is None. !!!\n")
    Tag = 4
    return 0

def GetUrl():
    # print(GetUrl.__name__, Queue.get())
    # http://www.uu234w.com/b8594/
    global Tag
    global GetUrlQueue
    global ParseQueue
    Gcount =0
    if GetUrlQueue:
        if (type(GetUrlQueue) is not queue.Queue) :
            return "Queue Type Invaild\n"
    GetLock  = threading.Lock()
    while 1:
        GetLock.acquire()
        if GetUrlQueue.empty():
            Tag = 1
            break
        url = GetUrlQueue.get()
        Gcount+=1
        print("[GET] : "+str(Gcount)+url+'\n')
        Sel = Selector(text = requests.get(url).content)
        time.sleep(1)
        ParseQueue.put(Sel)
        GetLock.release()
    return 0

def ParseHtml1():
    # print(ParseHtml1.__name__, end='\n')
    global Tag
    global ParseQueue
    global SqlQueue
    global WgetQueue
    Pcount =0
    Icount =0
    GetLock  = threading.Lock()
    while  1:
        if Tag == 1: break
        if ParseQueue.empty(): 
            time.sleep(1)
            continue
        GetLock.acquire()
        Sel = ParseQueue.get()
    #parse html 
        NovelUrlList = Sel.xpath("//li/span[2]/a[1]/@href").extract()
        NovelNameList = Sel.xpath("//li/span[2]/a[1]/text()").extract()
        NovelUrlList = [NovelRe.match(x).group(1) for x in NovelUrlList]
        NovelList = list(zip(NovelUrlList,NovelNameList))
        # http://down.uu234w.com/zip/%E9%A3%9E%E4%BA%91%E8%AF%80.zip
        DownUrlList = ["http://down.uu234w.com/zip/"+name+".zip" for name in NovelNameList]

        SqlQueue.put(NovelList)
        Icount+=len(NovelUrlList)
        Pcount+=1
        print("[PARSE]: "+str(Pcount)+" Items"+str(Icount)+"\n")
        for DownUrl in DownUrlList:
            WgetQueue.put(DownUrl)
        GetLock.release()
    Tag = 2
    return 0

def SqliteWrite():
    SqliteConn = sqlite3.connect(DBfile)
    global Tag
    GetLock  = threading.Lock()
    Scount =0
    while Tag != 2 :
        if SqlQueue.empty():
            time.sleep(1)
            continue
        GetLock.acquire()

        Data = SqlQueue.get()
        Scount +=len(Data)
        print("[SQL]: "+str(Scount)+"\n")
        try:
            SqliteConn.executemany(WriteCommand, Data)
            SqliteConn.commit()
        except Exception:
            print("Sqlite Write Error{} \n".format(Exception.__name__))
        GetLock.release()
    Tag = 3
    return 0
# threading.Thread().Tag = 0

if __name__ == "__main__":
    print("WorkSpace=={} ".format(os.getcwd()))
    
    '''
    TPageget = threading.Thread(name='GetPageThread',target = GetUrl)
    TParseget = threading.Thread(name='ParsePageThread',target = ParseHtml1)
    TSqlite = threading.Thread(name='SqlSyncThread',target=SqliteWrite)
    TGroup = [TPageget, TParseget, TSqlite]
    print("[START ALL Threads]\n")
    for wIndex in range(3):
        TGroup.append(threading.Thread(name = 'WgetT'+str(wIndex), target=WgetFile))
    for wGet in TGroup:
        wGet.setDaemon(True)
        wGet.start()

    for x in TGroup : x.join()
    '''
    conn = sqlite3.connect(DBfile)
    NovelNameList = conn.execute("select NovelName from uu234wQ;")
    
    for Novel in NovelNameList:
        print(Novel)
        WgetQueue.put("http://down.uu234w.com/zip/"+Novel[0]+".zip")
    TGroup =[]
    for wIndex in range(10):
        TGroup.append(threading.Thread(name = 'WgetT'+str(wIndex), target=WgetFile))
    for x in TGroup:
        x.setDaemon(True)
        x.start()
    for x in TGroup:
        x.join()

    print("Done ALL\n")
    



