from enum import unique
from genericpath import isfile
from operator import index
import os
from pprint import pprint
import json
import re
edk2dir = '/home/blacklee/WorkSpace/edk2'
sigle_comment = re.compile(r'^//.*')
f_comment = re.compile(r'^/\*\*.*')
a_comment = re.compile(r'.*\*\*/$')
section = re.compile(r'^\[(.*)\]')
key_value = re.compile(r'^(.*)=(.*)')
inf_fdf = re.compile(r'^INF\s+(.*\.inf)')
inf_dsc = re.compile(r'^\s*(.*\.inf)')
block = re.compile(r'')
re_bracket_block = re.compile(r'\{([^{]*)\}\s*\n')
re_front_bracket = re.compile(r'.*\{.*')
re_after_bracket = re.compile(r'.*\}.*')


def get_defines_intersections_union(filetype: str):
    with open('./python/PyEdk/edkfile/edk2.json', 'r') as fp:
        files = json.load(fp)
    defines_intersect = set()
    union_section = set()
    index = 0
    files = files.get('files')
    if(files):
        typefiles = files.get(filetype)
        if(typefiles):
            for file in typefiles.values():
                define = file.get('Defines')
                if(define):
                    tmp_keys = set(define.keys())
                    if(not index):
                        defines_intersect = tmp_keys
                        union_section = tmp_keys
                    defines_intersect = defines_intersect.intersection(
                        tmp_keys)
                    union_section = union_section.union(
                        tmp_keys)
                    index += 1
    return defines_intersect, union_section


def inf_deal(inf_file):
    defs = define_section_deal(inf_file)
    src = src_section_deal(inf_file)
    package = pkg_section_deal(inf_file)
    libs = lib_section_deal(inf_file)
    pcds = pcd_section_deal(inf_file)
    guids = guid_section_deal(inf_file)
    protocol = protocol_section_deal(inf_file)
    ppi = ppi_section_deal(inf_deal)
    depex = depex_section_deal(inf_file)


def get_intersection(filelist: list):
    '''get intersection from all typed file'''
    unique_section_name = set()
    index = 0
    for file in filelist:
        section_name = set(get_section_name(file))
        if(not index):
            unique_section_name = section_name
        if(not section_name):
            print("empty file section {}".format(file))
            return -1
        unique_section_name = unique_section_name.intersection(section_name)
        index += 1
        # pprint('INDEX:{} {}'.format(index, unique_section_name))
    return unique_section_name


def get_unic_section(filelist: list):
    ''''''
    unique_section_name = set()
    for file in filelist:
        section_name = get_section_name(file)
        if(not section_name):
            print("empty file section {}".format(file))
            return -1
        for section in section_name:
            if(section == ''):
                print('NULL')
            section = section.lower().replace('\"', '-')
            unique_section_name.add(section)
    pprint(unique_section_name)
    return unique_section_name


def get_section_name(filepath: str):
    '''get [XXX] section name from dec/dsc/fdf/inf file'''
    if(not os.path.isfile(filepath)):
        return -1
    # print(filepath)
    section_name = []
    section_str = ''
    with open(filepath, mode='r', encoding='utf-8') as fp:
        for line in fp.readlines():
            line = line.replace(' ', '')
            # pprint(line)
            is_section = section.search(line)
            if(is_section):
                section_str = is_section.group(1)
                sectlist = section_str.split(',')
                for sect in sectlist:
                    if(sect):
                        section_name.append(sect)
            else:
                # print('NOT Match')
                pass
    # pprint(section_name)
    return section_name


def get_section_struct(filepath: str):
    '''get section structure
       convert key:value to dict
       others to list
    '''
    if(not os.path.isfile(filepath)):
        return -1
    # print(filepath)
    section_blk = {}
    section_str = ''
    blks = ''
    bracket_count = 0
    is_comment = 0
    with open(filepath, mode='r', encoding='utf-8') as fp:
        for line in fp.readlines():
            is_section = 0
            raw_line = line.split('#')[0]
            line = line.replace(' ', '').split('#')[0]
            line = line.replace('\n', '')
            if(sigle_comment.match(line)):
                continue
            elif(f_comment.match(line)):
                if(a_comment.match(line)):
                    is_comment = 0
                    continue
                else:
                    is_comment = 1
                    continue
            elif(is_comment):
                if(a_comment.match(line)):
                    is_comment = 0
                    continue
                else:
                    continue
            elif(not line):
                continue
            # pprint(line)
            is_section = section.match(line)
            if(is_section):
                section_str = is_section.group(1)
                section_blk[section_str] = {'all': []}
                continue
                # sectlist = section_str.split(',')
                # for sect in sectlist:
                #     section_name.append(sect)
            t = line.count('{') - line.count('}')
            bracket_count += t
            is_key_value = key_value.match(line)
            if(bracket_count):
                blks = blks+raw_line
                continue
            elif(blks):
                blks = blks+raw_line
                section_blk[section_str]['all'].append(blks)
                blks = ''
                continue

            if(is_key_value and line.find('!if') < 0):
                blk = is_key_value.groups()
                blk = list(blk)
                if(bracket_count > 0):
                    if(line.count('{') > 0):
                        continue
                    blk[1] += raw_line
                    continue
                else:
                    section_blk[section_str][blk[0]] = blk[1]
                    is_key_value = 0
                    continue
            section_blk[section_str]['all'].append(line)
    return section_blk


def get_file_section_struct(filelist: list):
    '''
    get all files section struct
    '''
    files = {}
    for file in filelist:
        file_struct = get_section_struct(file)
        files[file] = file_struct
    return files


def sort_file_list(root):
    '''generate filelist dict & json file from root directiry'''
    if(not os.path.isdir(root)):
        return -1
    filelist = {}
    for nroot, dir, file in os.walk(root):
        if('Build' in nroot.split('/')):
            continue
        for item in file:
            if(item.find('.') < 0):
                continue
            filepath = nroot+'/'+item
            # pprint("PATH:{}".format(filepath))
            ext = item.lower().split('.')
            value = filelist.get(ext[-1], -1)
            if(value == -1):
                filelist[ext[-1]] = []
                filelist[ext[-1]].append(filepath)
            else:
                filelist[ext[-1]].append(filepath)
    with open('./python/PyEdk/edkfile/filelist.json', 'w+', encoding='utf-8') as fp:
        json.dump(filelist, fp, indent=2)
    return filelist


def files_deal(filelist, filetype):
    if(type(filelist) != dict or (filetype not in filelist)):
        return -1
    slist = filelist.get(filetype)
    for filepath in slist:
        if(filetype == 'dec'):
            dec_deal(filepath)
        elif(filetype == 'dsc'):
            dsc_deal(filepath)
        elif(filetype == 'inf'):
            inf_deal(filepath)
        elif(filetype == 'fdf'):
            fdf_deal(filepath)
        else:
            return -1
    return 0


def fdf_deal(filepath: str):
    if(not os.path.isfile(filepath)):
        return -1
    # print(filepath)
    file_json = {}
    section_name = ''
    with open(filepath, mode='r', encoding='utf-8') as fp:
        for line in fp.readlines():
            is_section = 0
            line = line.lstrip()
            if(not comment.match(line)):
                continue
            is_section = section.search(line)
            if(is_section):
                section_name = is_section.group(1)
                file_json[section_name] = {'all': []}
                continue
            line = line.replace(' ', '')
            is_key_value = key_value.search(line)
            if(is_key_value):
                keyarray = is_key_value.groups()
                file_json[section_name][keyarray[0]] = keyarray[1]

    return 1


UNION_SECT = 'union_sect'
INTERSECTION = 'intersections'
FILES = 'files'
UNION_DEFINES = 'union_defines'
INTERSECTION_DEFINES = 'intersection_defines'

if __name__ == "__main__":
    filelist = sort_file_list(edk2dir)
    sects = ['dec', 'fdf', 'dsc', 'inf']
    # sects = ['inf']
    edk2 = {}
    union_sect = {}
    for i in sects:
        sections = get_unic_section(filelist[i])
        union_sect[i] = list(sections)
    edk2[UNION_SECT] = union_sect
    # with open('./python/PyEdk/edkfile/cfg_union_section.json', 'w+', encoding='utf-8') as fp:
    #     json.dump(union_sect, fp, indent=2)
    #     unique_sect = {}
    intersection = {}
    for i in sects:
        sections = get_intersection(filelist[i])
        intersection[i] = list(sections)
    edk2[INTERSECTION] = intersection
    # with open('./python/PyEdk/edkfile/cfg_intersection.json', 'w+', encoding='utf-8') as fp:
    #     json.dump(intersection, fp, indent=2)
    files = {}
    for i in sects:
        files[i] = get_file_section_struct(filelist[i])
        # with open('./python/PyEdk/edkfile/'+i+'_file.json', 'w+', encoding='utf-8') as fp:
        #     json.dump(files[i], fp, indent=2)
    edk2[FILES] = files
    union_sect = {}
    intersect = {}
    for i in sects:
        intersection, union_section = get_defines_intersections_union(i)
        union_sect[i] = list(union_section)
        intersect[i] = list(intersection)
    edk2[UNION_DEFINES] = union_sect
    edk2[INTERSECTION_DEFINES] = intersect

    with open('./python/PyEdk/edkfile/edk2.json', 'w+', encoding='utf-8') as fp:
        json.dump(edk2, fp, indent=2)

    pprint('Over')
