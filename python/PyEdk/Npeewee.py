from peewee import *
import sys
import binascii
import uuid
import json
db_name = "BlackLee.db"
UNION_SECT = 'union_sect'
INTERSECTION = 'intersections'
FILES = 'files'
UNION_DEFINES = 'union_defines'
INTERSECTION_DEFINES = 'intersection_defines'

# import sqlite3
db = SqliteDatabase(db_name)

# con = db.connect()
# pcd = Table("Pcds", ("uid", "name", "package",
#                      "datatype", "package_id", "value"))
# pcd = pcd.bind(db)

# guid = Table("Guid", ("UUID", "GUID_TYPE"))
# guid = guid.bind(db)
edk2file = "./python/PyEdk/edkfile/edk2.json"


def get_spec_list(sections_name: list, sect_str: str):
    new_list = []
    for section_name in sections_name:
        if(section_name.lower().find(sect_str) > -1):
            new_list.append(section_name)
    return new_list


class BasicDriver(Model):
    file_guid = BinaryUUIDField()
    base_name = CharField()
    module_type = CharField()
    inf_version = CharField()

    class Meta:
        database = db


class BaseDriver(BasicDriver):
    version_str = CharField()

    class Meta:
        database = db


class Inf(BaseDriver):
    version = DoubleField()
    entry = CharField()
    raw = TextField()
    srcs = TextField()
    guids = TextField(null=False)
    pcds = TextField(null=False)
    depexs = TextField(null=False)
    ppis = TextField(null=False)
    protocols = TextField(null=False)
    libs = TextField(null=False)
    pkgs = TextField(null=False)

    class Meta:
        database = db


class Pcd(Model):
    pcd_uid = BinaryUUIDField(unique=True, primary_key=True)
    pcd_name = CharField(index=True)
    pcd_pkg = CharField()
    pcd_type = CharField()
    pcd_id = BigIntegerField(index=True)
    pcd_value = TextField()
    pcd_ntype = CharField()
    pcd_raw = TextField()

    class Meta:
        database = db


class Guid(Model):
    uid = IntegerField(primary_key=True, unique=True)
    g_uuid = BinaryUUIDField(index=True)
    uid_type = CharField()
    uid_name = CharField(index=True)
    uid_raw = TextField()

    class Meta:
        database = db


class Include(Model):
    uid = IntegerField(primary_key=True, unique=True)
    inc_type = CharField()
    inc_value = CharField()

    class Meta:
        database = db


class Libs(Model):
    uid = IntegerField(primary_key=True, unique=True)
    lib_name = CharField()
    lib_value = CharField()
    lib_type = CharField()

    class Meta:
        database = db


class Driver(Model):
    file_guid = BinaryUUIDField(primary_key=True)
    base_name = TextField()
    uni_file = TextField()
    module_type = CharField()
    version = IntegerField()
    entry = CharField()
    info_version = CharField()

    class Meta:
        database = db


def pcd_from_dec(filetype: str):
    with open('./python/PyEdk/edkfile/'+filetype+'_file.json', 'r') as fp:
        files = json.load(fp)
    for file in files.values():
        pcdlist = get_spec_list(file.keys(), 'pcd')
        for pcdsect in pcdlist:
            items = file.get(pcdsect)
            for item in items.get('all'):
                uid = uuid.uuid4().bytes
                pkg = item.split('.')[0]
                others = item.split('.')[-1]
                others = others.split('|')
                if(len(others) == 4):
                    name = others[0]
                    value = others[1]
                    type = others[2]
                    id = others[3]
                pcd = Pcd.create(pcd_name=name, pcd_pkg=pkg, pcd_type=type,
                                 pcd_id=id, pcd_value=value, pcd_uid=uid,
                                 pcd_ntype=pcdsect, pcd_raw=item)
                pcd.save()
    return 0


def guid_from_dec(filetype: str):
    with open('./python/PyEdk/edkfile/'+filetype+'_file.json', 'r') as fp:
        files = json.load(fp)
    guid_len_list = (8, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2)
    guidlist = []
    guid_key_list = ['guids', 'protocols', 'ppis']
    zero_index = 0
    for file in files.values():
        guidlist = []
        for key in guid_key_list:
            guidlist += get_spec_list(file.keys(), key)
        for guidsect in guidlist:
            items = file.get(guidsect)
            for key, value in items.items():
                if(key == 'all'):
                    continue
                name = key
                uid = value.lower().replace(',', '').replace(
                    '{', '').replace('}', '').replace('0x', '')
                if(len(uid) < 32):
                    uid = value.replace(
                        '{', '').replace('}', '').replace('0x', '')
                    uid = uid.split(',')
                    if(int(uid[0], base=16) == 0):
                        zero_index += 1
                        uid[0] = str(zero_index)
                    maplist = tuple(map(len, uid))
                    d = list(zip(guid_len_list, maplist))
                    # d = list(map(lambda x: x[0] - x[1], d))
                    d = [x[0] - x[1] for x in d]
                    uid = list(zip(d, uid))
                    # uid = list(map(lambda x: '0'*x[0]+x[1], uid))
                    uid = ['0'*x[0]+x[1] for x in uid]
                    uid = ''.join(uid)
                    uid = binascii.a2b_hex(uid.encode())
                else:
                    uid = binascii.a2b_hex(uid.encode())
                type = guidsect
                raw = '{}:{}'.format(key, value)
                guid = Guid.create(g_uuid=uid, uid_type=type, uid_name=name,
                                   uid_raw=raw)
                guid.save()
    return 0


def include_from_dec(filetype: str):
    with open('./python/PyEdk/edkfile/'+filetype+'_file.json', 'r') as fp:
        files = json.load(fp)
    key_list = ['includes']
    for file in files.values():
        sect_list = []
        for key in key_list:
            sect_list += get_spec_list(file.keys(), key)
        for sect in sect_list:
            items = file.get(sect)
            itemlist = items.get('all')
            for item in itemlist:
                inc = Include.create(inc_type=sect, inc_value=item)
                inc.save()
    return 0


def libs_from_dec(filetype: str):
    with open('./python/PyEdk/edkfile/'+filetype+'_file.json', 'r') as fp:
        files = json.load(fp)
    key_list = ['libraryclasses']
    for file in files.values():
        sect_list = []
        for key in key_list:
            sect_list += get_spec_list(file.keys(), key)
        for sect in sect_list:
            items = file.get(sect)
            itemlist = items.get('all')
            for item in itemlist:
                name = item.split('|')
                value = name[-1]
                name = name[0]
                lib = Libs.create(
                    lib_name=name, lib_value=value, lib_type=sect)
                lib.save()
    return 0


'''
def items_from_inf(filetype: str):
    keys = ('Sources', 'Guids', 'Pcds', 'Depex', 'Ppis',
            'Protocols', 'LibraryClasses', 'Packages', )

    with open('./python/PyEdk/edkfile/'+filetype+'_file.json', 'r') as fp:
        files = json.load(fp)
    for file in files.values():
        Defines = file.get('Defines')
        Sources = file.get('Sources')
        uuid = binascii.a2b_hex(Defines.get(
            'FILE_GUID').replace('-', '').lower().encode())
        Guids = file.get('Guids')
        if(not Guids):
            Guids['all'] = ''
        Packages = file.get('Packages')
        Librarys = file.get('LibraryClasses')
        Protocols = file.get('Protocols')
        Ppis = file.get('Ppis')
        Depex = file.get('Depex')
        Pcds = file.get('Pcd')
        a_dict = {
            'guid' = uuid,
            'inf_version' = Defines.get('INF_VERSION'),
            'base_name' = Defines.get('BASE_NAME'),
            'type' = Defines.get('MODULE_TYPE'),
            version = float(Defines.get('VERSION_STRING')),
            entry = Defines.get('ENTRY_POINT'),
            raw = str(Defines),
            # srcs=str(Sources.get('all')),
            srcs = '',
            guids = '',
            pcds = '',
            depexs = '',
            ppis = '',
            protocols = '',
            libs = '',
            pkgs = '',
        }
        inf = Inf.create(
            guid=uuid,
            inf_version=Defines.get('INF_VERSION'),
            base_name=Defines.get('BASE_NAME'),
            type=Defines.get('MODULE_TYPE'),
            version=float(Defines.get('VERSION_STRING')),
            entry=Defines.get('ENTRY_POINT'),
            raw=str(Defines),
            # srcs=str(Sources.get('all')),
            srcs='',
            guids='',
            pcds='',
            depexs='',
            ppis='',
            protocols='',
            libs='',
            pkgs='',
            #   guids=str(Guids.get('all')),
            #   pcds=str(Pcds.get('all')),
            #   depexs=str(Depex.get('all')),
            #   ppis=str(Ppis.get('all')),
            #   protocols=str(Protocols.get('all')),
            #   libs=str(Librarys.get('all')),
            #   pkgs=str(Packages.get('all'))
        )
        inf.save()
    return 0
'''

SQL_CREATE_TABLE = '''
    CREATE TABLE {} (
    {}
    );
    '''
SQL_DROP_TABLE = '''
    DROP TABLE {};
    '''
SQL_INSERT_ITEM = '''
    INSERT INTO {} 
    {}
    VALUES {};
    '''


def get_table_column(filetype: str):
    with open(edk2file, 'r') as fp:
        files = json.load(fp)
    union_sections = files.get(UNION_SECT).get(filetype)
    union_defines = files.get(UNION_DEFINES).get(filetype)
    files = None
    union_sections = set(union_sections).union(union_defines)
    union_sections = [x.replace('\"', '-') for x in union_sections]
    inf_sects = ','.join([f"\"{x}\" TEXT" for x in union_sections])
    create_inf_table = SQL_CREATE_TABLE.format(filetype, inf_sects)
    print(create_inf_table)
    return create_inf_table


def fill_table(filetype: str):
    with open(edk2file, 'r') as fp:
        files = json.load(fp)
    union_sections = files.get(UNION_SECT).get(filetype)
    union_defines = files.get(UNION_DEFINES).get(filetype)
    files = files.get('files').get(filetype)
    columns = []
    columns_value = []
    for file in files.values():
        columns = []
        columns_value = []

        for key in file.keys():
            if(key == 'Defines'):
                continue
            key_lower = key.replace('\"', '-').lower()
            if key_lower in union_sections:
                columns.append(key_lower)
                columns_value.append(
                    '\n'.join(file.get(key).get('all', 'NULL')))
        defines = file.get('Defines')
        if defines:
            for sect in union_defines:
                if(sect == 'all'):
                    continue
                if sect in defines.keys():
                    columns.append(sect)
                    columns_value.append(defines.get(sect, 'NULL'))
        columns = str(tuple(columns))
        columns_value = str(tuple(columns_value))
        db.execute_sql(SQL_INSERT_ITEM.format(
            filetype, columns, columns_value))
        db.commit()
    return 0


if __name__ == '__main__':
    print("Hello Peewee")
    sects = ['dec', 'fdf', 'dsc', 'inf']
    for x in sects:
        if(db.table_exists(x)):
            db.execute_sql(SQL_DROP_TABLE.format(x))
        db.execute_sql(get_table_column(x))
        db.commit()
        fill_table(x)
    db.create_tables(Pcd)
    # db.drop_tables([Pcd, Guid, Include, Libs, Inf])
    # db.commit()
    # db.create_tables([Pcd, Guid, Include, Libs, Inf])
    # db.commit()
    # # pcd_from_dec('dec')
    # guid_from_dec('dec')
    # include_from_dec('dec')
    # libs_from_dec('dec')
    # items_from_inf('inf')
    # a = get_defines_intersections('inf')
    # print(a)
    # a = get_defines_intersections('dec')
    # print(a)
    # a = get_defines_intersections('dsc')
    # print(a)
    db.commit()
    db.close()
    print("Over")
