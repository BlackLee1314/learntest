-- SQLite
-- INSERT TABLE
CREATE TABLE FileList(
  ID INTEGER PRIMARY KEY,
  FileID BLOB,
  FilePath TEXT
);
-- INSERT ITEM
INSERT INTO FileList
VALUES (0x11111111, "ME.TEXT");
-- Drop table
DROP TABLE FileList;
--clear table
DELETE FROM FileList;
--select record
SELECT *
FROM FileList
WHERE FileID LIKE '0000%';
--select record
SELECT *
FROM FileList
WHERE ID = 7;
--select record
SELECT *
FROM FileList;