import scrapy
from scrapy import item
from BlackSpyder.items import Uu234wItem

class Uu234wSpider(scrapy.Spider):
    name = 'uu234w'
    allowed_domains = ['uu234w.com']
    start_urls = ['http://www.uu234w.com/quanben/list_1.html']

    def parse(self, response):
        BookList = response.xpath('/html/body/div[4]/div[1]/ul/li')
        for Book in BookList:
            # /html/body/div[4]/div[1]/ul/li[1]/span[2]/text()
            BookType = Book.xpath('./span[2]/text()').extract_first()
            # /html/body/div[4]/div[1]/ul/li[1]/span[2]/a[1]
            BookName = Book.xpath('./span[2]/a[1]/text()').extract_first()
            BookUrl = Book.xpath('./span[2]/a[1]/@href').extract_first()
            # /html/body/div[4]/div[1]/ul/li[1]/span[4]
            BookSize = Book.xpath('./span[4]/text()').extract_first()
            # /html/body/div[4]/div[1]/ul/li[1]/span[5]/a
            BookAuthor = Book.xpath('./span[5]/a/text()').extract_first()
            Item = Uu234wItem()
            Item['Name'] = BookName
            Item['BookType'] = BookType
            Item['Size']= BookSize
            Item['Author'] =BookAuthor
            # Item['DownUrl'] = ''
            if BookUrl:
                yield scrapy.Request(url=BookUrl, meta={'item':Item},callback=self.parseDownUrl)
            else:
                self.logger.warning("No Book Url Fount %s\n", response.url)
        # /html/body/div[4]/div[1]/div[2]/div/a[9]
        NextPage = response.xpath('//div[@class="pages"]/a')[-1].xpath('./@href').extract_first()
        if NextPage:
            yield scrapy.Request(url=NextPage,callback=self.parse)
    def parseDownUrl(self, response):
        Item = response.meta['item']
        # //*[@id="download"]/div[2]/div[1]/a[1]
        #download > div.downmain > div.downmenu > a:nth-child(1)
        DownUrl = response.xpath('//*[@id="download"]/div[2]/div[1]/a[1]/@href').get()
        if DownUrl:
            Item['DownUrl'] = DownUrl
            yield Item
        else:
            self.logger.warning('[DOWNURL] Not found %s', response.url)