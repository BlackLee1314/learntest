# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import sqlite3

class BlackspyderPipeline:
    def process_item(self, item, spider):
        return item
class Uu234wPipeline:
    def __init__(self,SqlitePath):
        self.SqlitePath = SqlitePath
        self.conn = sqlite3.connect(self.SqlitePath)
        
    def start_spider(self, spider):
        # self.conn = sqlite3.connect(self.SqlitePath)
        pass

    def close_spider(self, spider):
        self.conn.close()

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            SqlitePath = crawler.settings.get('SQLITEPATH')
        )
    def process_item(self, item, spider):
        self.conn.execute('''INSERT INTO uu234wBookTable
                        (Name, "Size", BookType, Author, DownUrl)
                        VALUES(?, ?, ?, ?, ?);''',
                        (item['Name'],item['Size'], item['BookType'], item['Author'],item['DownUrl']))
        self.conn.commit()
        return item
