# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BlackspyderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
class Uu234wItem(scrapy.Item):
    Name = scrapy.Field()
    BookType = scrapy.Field()
    Size = scrapy.Field()
    Author = scrapy.Field()
    DownUrl = scrapy.Field()
