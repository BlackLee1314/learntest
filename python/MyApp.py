import os
import hashlib
import sqlite3
from queue import Queue
rootdir = os.getcwd()
print(rootdir)
q = Queue(maxsize=1000)
con = None
cur = None


def SqliteOpen(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    print("Global Sqlite DataBase Opened")
    return (con, cur)


def SqliteClose(con, cur):
    cur.close()
    con.close()
    print("Global Sqlite DataBase Closed")


# insert item
SqlInsertItem = '''
INSERT INTO FileList
VALUES (?, ?, ?);
'''
Items1000 = []


def GetFileMd5(fname):
    m = hashlib.sha256()  # 创建md5对象
    with open(fname, 'rb') as fobj:
        while True:
            data = fobj.read(4096)
            if not data:
                break
            m.update(data)  # 更新md5对象
        return m.hexdigest()  # 返回md5对象


def LogFilesMd5ToDataBase(rootdir, cur):
    Index = 0
    for root, dir, file in os.walk(rootdir, topdown=True):
        print("R:{} D:{} F:{}\n".format(root, dir, file))
        if(root.find("/.") > 0):
            continue
        if 'Build' in root.split('/'):
            continue
        for item in file:
            print(type(item))
            filepath = "{}/{}".format(root, item)

            print(filepath, os.lstat(filepath).st_size, end='\n')
            if(os.lstat(filepath).st_size > 16):
                Md5 = GetFileMd5(filepath)
            else:
                Md5 = b"00"*0x20
            # q.put((Md5, filepath))
            Index += 1
            cur.execute(SqlInsertItem, (Index, Md5, filepath))
            con.commit()
    return 0


DBfile = rootdir+'/BlackLee.db'
rootdir = '/mnt/d/WorkSpace/edk2'
# -- INSERT TABLE
create_table = '''
CREATE TABLE FileList(
  ID INTEGER PRIMARY KEY,
  FileID BLOB,
  FilePath TEXT
);
'''
# -- Drop table
drop_table = '''DROP TABLE FileList;'''
# --clear table
clear_table = '''DELETE FROM FileList;'''


if __name__ == '__main__':
    con, cur = SqliteOpen(DBfile)
    cur.execute(clear_table)
    con.commit()
    LogFilesMd5ToDataBase(rootdir, cur)
    SqliteClose(con, cur)
