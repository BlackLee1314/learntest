# WuZhou

1. RTC 唤醒


   | 配置/结果 | day | hour | min | sec | ctl | sta | Result |
   | - | - | - | - | - | - | - | - |
   | 1 | 81 | 0 | 4 | 0 | 1d | 0 | 信号拉低并保持 |
   | 2 | 1 | 0 | 8 | 0 | 1d | 0 | 信号拉低后拉高 |
   | 3 | 80 | 0 | 4 | 0 | 1d | 0 | 信号拉低并保持 |

   触发唤醒需要一个拉低信号，发送拉低信号后可以通过启动过程种重置中断信号来保证下次的中断信号会恢复为高（可拉低状态）。
2. Setup界面卡死
   1. Assert 出现的地方如图，**Core2.0 中行为2375**
      ![image](WuzhouSetupAssert2348.png)
   2. 复现手法
      移除PlatformDxe.c 中设置Variable SysPd 的所有语句。
      此时Variable SysPd应该为SystemPassword.c 中SystemPasswordInit()初始化后的值。
      编译并烧录后中文下移动至安全页面即可复现卡死的现象。

   ### 原因：

   SystemPassword.c 中1057-1066行会用随机值初始化Variable SysPd,


   ```C
       RandomSeed(NULL, 0);
       for (Index = 0; Index < SYSTEM_PASSWORD_LENGTH; Index++) {
         RandomBytes((UINT8 *)&SetupPassword.Admin[Index], 2);
       }

       RandomSeed(NULL, 0);
       for (Index = 0; Index < SYSTEM_PASSWORD_LENGTH; Index++) {
         RandomBytes((UINT8 *)&SetupPassword.PowerOn[Index], 2);
       }
   ```
   在安全页面中,引用了 `SYS_PD.Admin`

   ```vfr
   password varid  = SYS_PD.Admin, 
           prompt      = STRING_TOKEN(STR_SET_ADMIN_PASSWORD),
           help        = STRING_TOKEN(STR_ADMIN_PASSWORD_HELP),
           flags       = 0 | RESET_REQUIRED | INTERACTIVE, key = SEC_KEY_ADMIN_PD,
           minsize     = 8,
           maxsize     = SYSTEM_PASSWORD_LENGTH,
       endpassword;
   ```
   在显示时，拿取相应的Variable区域时它是通过GetToken()的方式**SetupBrowserDxe/Presentation.c line2372**拿取的,
   在语言为中文时，访问相应的资源会判断之前的随机值区域是否为宽字符，宽字符会填充**\wide\narrow**,使字串长度改变，进而导致**2377**行Assert 出现卡死现象。细节如下图。
   ![image](Wuzhou_SysPd_dump.png)
